package main

import (
	m "./middleware"
	h "./handler"
)

func initRoutes() {
	public := router.Group("/api/v1")
	public.Use(m.Cors())
	{
		public.GET("/ping", h.Ping)
		public.GET("/articles", h.AllArticles)
		public.GET("/article/:id", h.Article)
		public.PUT("/article/:id", h.UpdateArticle)
	}

	authorized := router.Group("/api/v1")
	authorized.Use(m.AuthMiddleware())
	authorized.Use(m.Cors())
	{
		authorized.GET("/", h.Hello)
		authorized.POST("/articles", h.CreateArticle)
		authorized.DELETE("/articles", h.RemoveAllArticles)
		authorized.DELETE("/article/:id", h.DeleteArticle)
	}
}
