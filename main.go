package main

import (
	"github.com/gin-gonic/gin"
	"./models"
)

var router *gin.Engine

func main() {
	defer models.DB.Close()

	router = gin.Default()

	router.Use(gin.Logger())

	initRoutes()

	router.Run(":8088")
}
