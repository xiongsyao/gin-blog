package models

import (
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/go-sql-driver/mysql"

	"../utils"
)

var DB *gorm.DB

func init() {
	addr := utils.GetEnv("MYSQL_ADDR", "127.0.0.1:3306")
	name := utils.GetEnv("MYSQL_DB", "gin_blog")
	user := utils.GetEnv("MYSQL_USER", "root")
	pwd := utils.GetEnv("MYSQL_PWD", "123456")

	db, err := gorm.Open("mysql", user + ":" + pwd + "@tcp(" + addr + ")/" + name + "?charset=utf8")
	if err != nil{
		log.Fatal(err)
	}
	db.DB().SetMaxIdleConns(10)
	db.DB().SetMaxOpenConns(100)
	DB = db
	if err := DB.DB().Ping(); err != nil {
		log.Fatal(err)
	}
}