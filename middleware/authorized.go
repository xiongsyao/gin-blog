package middleware

import (
	"fmt"
	"github.com/gin-gonic/gin"
)


func AuthMiddleware() gin.HandlerFunc{
	return func(c *gin.Context) {
		if username, err := c.Request.Cookie("username"); err != nil {
			fmt.Println("用户没有登录")
		} else {
			fmt.Println(username)
		}
		c.Next()
	}
}