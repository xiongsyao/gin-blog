CREATE TABLE `articles` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `title` varchar(127) NOT NULL DEFAULT '',
    `content` text NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;